#include "game.hpp"

#include <fstream>
#include <iostream>
#include <stdint.h>
#include <string>

using namespace std;

int main(int argc, char* argv[]) {
  if (argc < 2) {
    cout << "Usage: " << argv[0] << " <file>" << endl;
    return 1;
  }

  ifstream f(argv[1]);

  if (!f.is_open()) {
    cout << "Cannot find " << argv[1] << endl;
    return 1;
  }

  u8 boardToSet[SDIM][SDIM];

  for (u8 i = 0; i < SDIM; i++) {
    for (u8 j = 0; j < SDIM; j++) {
      string elemStr;
      if (!(f >> elemStr)) {
        cout << "Error reading input for input cell " << to_string(i) << ", "
             << to_string(j) << endl;
        return 1;
      }
      try {
        boardToSet[i][j] = stoi(elemStr);
      } catch (invalid_argument) {
        cout << "Encountered invalid number for input cell " << to_string(i)
             << ", " << to_string(j) << endl;
        return 1;
      }
      if (boardToSet[i][j] > SDIM) {
        cout << "While reading input, maximum size exceeded for cell "
             << to_string(i) << ", " << to_string(j) << endl;
        return 1;
      }
    }
  }

  Board b(boardToSet);
  b.isMaster = true;
  int status = b.solve();
  if (status >> 16) {
    b.show();
    cout << "Puzzle is inconsistent. Cell " << to_string((status >> 8) & 0xFF)
         << ", " << to_string(status & 0xFF)
         << " has no legal value. This is what I solved so far. " << endl;
    return status >> 16;
  }
  b.show();
  if (!b.isSolved()) {
    cout << "I couldn't solve this puzzle! Either the solution is ambiguous, "
            "or I have more to learn!"
         << endl;
  }
  return 0;
}