#include "game.hpp"

#include <assert.h>
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

inline void copyPossible(bool* a, const bool* b) {
  for (u8 i = 0; i < SDIM; i++) {
    a[i] = b[i];
  }
}

/*
 * Sets the left array to the logical difference of the two such that the left
 * can only contain elements which it originally posessed
 */
inline void possibilityDifference(bool* l, const bool* r) {
  for (u8 i = 0; i < SDIM; i++) {
    l[i] &= !r[i];
  }
}

u8 singlePossibility(bool* a) {
  u8 t = 0;
  for (u8 i = 0; i < SDIM; i++) {
    if (a[i]) {
      if (t) {
        return 0;
      }
      t = i + 1;
    }
  }
  return t;
}

Cell::Cell(u8 v) {
  if (!v) {
    for (u8 i = 0; i < SDIM; i++) {
      this->possible[i] = true;
    }
  }
  this->_value = v;
}

u8 Cell::value() {
  if (_value) {
    return _value;
  }
  bool any = false;
  u8 t = 0;
  for (u8 i = 0; i < SDIM; i++) {
    any |= this->possible[i];
    if (this->possible[i]) {
      if (t) {
        t = 0;
        break;
      }
      t = i + 1;
    }
  }
  _value = t;
  if (!any) {
    _value = ~0;
  }
  return _value;
}

bool Cell::impossible(u8 x) {
  assert(x <= SDIM && x > 0);
  bool ov = possible[x - 1];
  possible[x - 1] = false;
  return ov;
}

bool Cell::setValue(u8 x) {
  u8 ov = _value;
  _value = x;
  return _value != ov;
}

Board::Board(u8 in[SDIM][SDIM]) {
  for (u8 r = 0; r < SDIM; r++) {
    for (u8 c = 0; c < SDIM; c++) {
      this->cells[r][c] = new Cell(in[r][c]);
    }
  }
}

void Board::show() {
  for (u8 row = 0; row < SDIM; row++) {
    for (u8 col = 0; col < SDIM; col++) {
      u8 v = this->cells[row][col]->value();
      if (v <= SDIM && v > 0) {
        cout << to_string(this->cells[row][col]->value()) << " ";
      } else {
        cout << "X ";
      }
      if (!((col + 1) % SDIM_SQRT) && col < SDIM - 1)
        cout << "| ";
    }
    if (!((row + 1) % SDIM_SQRT) && row  < SDIM - 1)
      cout << endl << "---------------------";
    cout << endl;
  }
}

bool Board::exclusion(u8 row, u8 col, u8 value) {
  bool mut = false;
  for (u8 i = 0; i < SDIM; i++) {
    mut |= this->cells[row][i]->impossible(value);
    mut |= this->cells[i][col]->impossible(value);
  }
  u8 rbase = row - (row % SDIM_SQRT);
  u8 cbase = col - (col % SDIM_SQRT);
  for (u8 r = 0; r < SDIM_SQRT; r++) {
    for (u8 c = 0; c < SDIM_SQRT; c++) {
      Cell* tc = this->cells[rbase + r][cbase + c];
      mut |= tc->impossible(value);
    }
  }
  return mut;
}

bool Board::deduction(u8 row, u8 col) {
  bool lpossible[SDIM];

  // row
  copyPossible(lpossible, this->cells[row][col]->possible);
  for (u8 c = 0; c < SDIM; c++) {
    if (unlikely(c == col)) {
      continue;
    }
    u8 v = this->cells[row][c]->value();
    if (v) {
      lpossible[v - 1] &= !v;
    } else {
      possibilityDifference(lpossible, this->cells[row][c]->possible);
    }
  }
  if (this->cells[row][col]->setValue(singlePossibility(lpossible))) {
    return true;
  }

  // col
  copyPossible(lpossible, this->cells[row][col]->possible);
  for (u8 r = 0; r < SDIM; r++) {
    if (unlikely(r == row)) {
      continue;
    }
    u8 v = this->cells[r][col]->value();
    if (v) {
      lpossible[v - 1] &= !v;
    } else {
      possibilityDifference(lpossible, this->cells[r][col]->possible);
    }
  }
  if (this->cells[row][col]->setValue(singlePossibility(lpossible))) {
    return true;
  }

  // block
  copyPossible(lpossible, this->cells[row][col]->possible);
  u8 rbase = row - (row % SDIM_SQRT);
  u8 cbase = col - (col % SDIM_SQRT);
  for (u8 r = 0; r < SDIM_SQRT; r++) {
    for (u8 c = 0; c < SDIM_SQRT; c++) {
      if (unlikely(r == row && c == col)) {
        continue;
      }
      u8 v = this->cells[rbase + r][cbase + c]->value();
      if (v) {
        lpossible[v - 1] &= !v;
      } else {
        possibilityDifference(lpossible,
                              this->cells[rbase + r][cbase + c]->possible);
      }
    }
  }
  return this->cells[row][col]->setValue(singlePossibility(lpossible));
}

bool Board::isSolved() {
  for (u8 r = 0; r < SDIM_SQRT; r++) {
    for (u8 c = 0; c < SDIM_SQRT; c++) {
      if (!this->cells[r][c]->value()) {
        return false;
      }
    }
  }
  return true;
}

uint32_t Board::solve() {
  bool mut;
  do {
    mut = false;
    for (u8 r = 0; r < SDIM; r++) {
      for (u8 c = 0; c < SDIM; c++) {
        u8 v = this->cells[r][c]->value();
        if (unlikely(v == (u8)~0)) {
          return 1 << 16 | r << 8 | c;
        }
        if (v) {
          mut |= exclusion(r, c, v);
        } else {
          mut |= deduction(r, c);
        }
      }
    }
  } while (mut);
  if (this->isSolved()) {
    return 0;
  }
  // The simple methods have failed, so resort to an intelligent bruteforce
  // solution
  if (isMaster) {
    u8 newBoard[SDIM][SDIM];
    for (u8 r = 0; r < SDIM; r++) {
      for (u8 c = 0; c < SDIM; c++) {
        newBoard[r][c] = this->cells[r][c]->value();
      }
    }
    for (u8 r = 0; r < SDIM; r++) {
      for (u8 c = 0; c < SDIM; c++) {
        if (newBoard[r][c]) {
          continue;
        }
        for (u8 tryIndex = 0; tryIndex < SDIM; tryIndex++) {
          if (!cells[r][c]->possible[tryIndex]) {
            continue;
          }
          newBoard[r][c] = tryIndex + 1;
          Board nb(newBoard);
          uint32_t status = nb.solve();
          if (!status) {
            for (u8 x = 0; x < SDIM; x++) {
              for (u8 y = 0; y < SDIM; y++) {
                this->cells[x][y]->setValue(nb.cells[x][y]->value());
              }
            }
            return 0;
          }
          newBoard[r][c] = 0;
        }
      }
    }
  }
  // The puzzle must be unsolvable, or the algorithm is broken
  return 2 << 16;
}