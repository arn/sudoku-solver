#include <bits/stdint-uintn.h>
#include <stdint.h>

#define u8 uint_fast8_t

#define likely(x) __builtin_expect(x, 1)
#define unlikely(x) __builtin_expect(x, 0)

#define SDIM 9
#define SDIM_SQRT 3

class Cell {
  u8 _value;

public:
  bool possible[SDIM];
  Cell(u8);
  bool impossible(u8);
  bool isResolved();
  u8 value();
  bool setValue(u8);
};

class Board {
public:
  bool isMaster = false;

  Board(u8[SDIM][SDIM]);
  Cell* cells[SDIM][SDIM];
  bool exclusion(u8 row, u8 col, u8 v);
  bool deduction(u8 row, u8 col);
  bool isSolved();
  void show();
  uint32_t solve();
};
