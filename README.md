# Sudoku Solver

A very simple Sudoku solver written in C++. I have not done any formal analysis
of the algorithm's ability to solve ALL solvable Sudoku, but it has been confirmed
to work on several samples accross various difficulties (although this is not impressive, as is discussed later).
The main next steps for this project are to acquire many test cases, both to test its accuracy
and benchmark any optimizations I make later.


This project was born after I started solving Sudoku puzzles and realized that I
would better enjoy spending my time writing this. I quickly realized that while
the basic logic of the puzzle was easy to implement, it was hard to capture
certain aspects of the solving strategy. In particular, I couldn't find
a good/efficient way to determine the case when a small set of cells MUST
contain a certain number, and therefore conclusions can be made about other
cells in the same unit.  

To solve this issue, I simply resorted to an intelligent brute force solution,
where the system makes a guess about an unknown cell, then tries to solve the
puzzle based on that assumption. This was found to be much more efficient
(both in terms of development effort and computation time) than my original 
algorithm for detecting this case intelligently. However, this is an
uninteresting solution from a logic perspective, which is what Sudoku is all about at the end of the day.