PROG_NAME := sudoku
CXX := clang++

SRC_DIR := ./src
OBJ_DIR := ./obj

LD_FLAGS := 
CXX_FLAGS := -I$(SRC_DIR)

SRC_FILES := $(wildcard $(SRC_DIR)/*.cpp)
OBJ_FILES := $(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRC_FILES))

all: CXX_FLAGS += -O2
all: dirs $(PROG_NAME)

debug: CXX_FLAGS += -g3
debug: CXX_FLAGS += -Wall -Wextra -pedantic
debug: clean dirs $(PROG_NAME)

stripped: $(PROG_NAME)
	sstrip -z $(PROG_NAME)

$(PROG_NAME): $(OBJ_FILES)
	$(CXX) $(LD_FLAGS) -o $@ $^

dirs: obj

obj:
	mkdir -p $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(CXX_FLAGS) -c -o $@ $< 

clean:
	rm -rf $(PROG_NAME) obj/ 
